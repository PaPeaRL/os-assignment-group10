#include <stdio.h>
#include <time.h>
#include <pthread.h>
int state;
struct Node{				// one node contain data of one charactor and pointer to next node
	char data;
	struct Node *next;
};

struct Node *root,*read,*write; // global variable, so no need to passing argument in add and remove item
pthread_mutex_t lock;
int successor;
int requestLeft;
int add,rem,timeout;

void GenNode(struct Node *root,int size){				// malloc 1 to 1000 Node
		struct Node *a;
		a = root;
		int i;
		for(i=0;i<size-1;i++){
			a->next = (struct Node*)malloc(sizeof(struct Node));
			a->data = NULL;
			a = a->next;
		}
		a->next = root;
}

void add_item(char dat){
	//printf("\nadded%d",successor);
	write->data = dat;
	write = write->next;
	add++;
	/* the function must return something - NULL will do */
	return NULL;
}
void remove_item(){
	//printf("\nremoved");
	read->data = NULL;
	read = read->next;
	rem++;
	return NULL;
}


void *append(){
	clock_t start,end;
	while(1){
		start = clock();
		while(state){
            if(requestLeft <=0){
            pthread_exit(NULL);
			}
			end = clock();
			if((float)(end - start)<50){    //1000000 = 1 second
				if(write->data==NULL){
		    		pthread_mutex_lock(&lock);
                    if(requestLeft <=0){
                        pthread_mutex_unlock(&lock);
                        pthread_exit(NULL);
                    }
                    else{
                        if(write->data==NULL && state && requestLeft >0){
                            add_item('a');
                            requestLeft--;
                        }
                        else {
                            requestLeft--;
                        	timeout++;
                        }
		    		}
                    pthread_mutex_unlock(&lock);
				}
				else{
					state=0;break;
				}
			}
			else{requestLeft--;timeout++;break;}
			}
		if(requestLeft <=0){
			pthread_exit(NULL);
		}
	}
}

void *removea(){
	clock_t start,end;
	while(1){
		start = clock();
		while(!state){
            if(requestLeft <=0){
            pthread_exit(NULL);
			}
			end = clock();
			if((float)(end - start)<50){    //1000000 = 1 second
				if(read->data!=NULL){
		    		pthread_mutex_lock(&lock);
		    		if(requestLeft <=0){
                        pthread_mutex_unlock(&lock);
                        pthread_exit(NULL);
                    }
                    else{
                        if(read->data!=NULL && !state){
                            remove_item();
                            successor++;
                        }
                        else {
                        	timeout++;
                        }
                    }
		    		pthread_mutex_unlock(&lock);
				}
				else{
					state=1;break;
				}
			}
			else{timeout++;break;}
		}
        if(requestLeft <=0){
				pthread_exit(NULL);
        }
	}
}


int main(){
	int buffersize;
	int pdc,csm;
	root = (struct Node*)malloc(sizeof(struct Node));
    int request ;
    state=1;
    printf("Enter Buffersize : ");
    scanf("%d",&buffersize);

    printf("Enter Producer thread : ");
    scanf("%d",&pdc);

    printf("Enter Consumer thread : ");
    scanf("%d",&csm);

    printf("Enter total Request : ");
    scanf("%d",&request);

	pthread_t Producer[pdc];
	pthread_t Consumer[csm];

    // basic setting
    requestLeft = request;
    GenNode(root,buffersize);
    successor = 0;
    clock_t start,end;
    read = root;		// set pointer
    write = root;
    start = clock();
    pthread_mutex_init(&lock, NULL);
    int i =0;

    i=0;
    //thread programming start here
    while(i < sizeof(Producer)/sizeof(Producer[0])){
    pthread_create(&Producer[i],NULL,append,NULL);
    i++;
	}

	i=0;
	while(i < sizeof(Consumer)/sizeof(Consumer[0])){
    pthread_create(&Consumer[i],NULL,removea,NULL);
    i++;
	}

	for(i=0;i<sizeof(Producer)/sizeof(Producer[0]);i++)
	pthread_join(Producer[i],NULL);
	for(i=0;i<sizeof(Consumer)/sizeof(Consumer[0]);i++)
	pthread_join(Consumer[i],NULL);
    end = clock();
    double different =  (double)(end-start)/CLOCKS_PER_SEC;
    printf ("\nProducers %d, Consumers %d\n",sizeof(Producer)/sizeof(Producer[0]),sizeof(Consumer)/sizeof(Consumer[0]));
    printf("Buffer size %d\nRequests %d\n",buffersize,request);
    printf("Successfully consumed %d requests (%.2lf%%)\n",successor,((double)successor/request)*100);
    printf("Elapsed Time: %.2lf s\nThroughput: %.2lf successful requests/s\n",different,successor/different);
    //printf("add: %d       rem: %d    timeout: %d",add,rem,timeout);

    for(i=0;i<buffersize;i++){   // free those poor soul innocent memory ~
        write = root;
        root = root->next;
        free(write);
    }
    return 0;
}
